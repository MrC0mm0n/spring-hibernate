package my.pack.app;

import my.pack.app.dao.ItemDAO;
import my.pack.app.model.Item;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class MainController {

	private static final Logger logger = LoggerFactory
			.getLogger(MainController.class);

	public static void main(String[] args) {
		logger.info("Starting Application");

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"META-INF/spring/app-context.xml");
		MainController mainCnrtl = context.getBean(MainController.class);
		mainCnrtl.callService(context);

		logger.info("Closing Application");
	}

	@Autowired
	ItemDAO itemDAO;

	public void callService(ApplicationContext context) {
		Item item = new Item();
		item.setName("Samsung Galaxy S");
		item.setPrice(5.88);
		item.setDescription("This is a smartphone");

		itemDAO.insertItem(item);
	}

}
