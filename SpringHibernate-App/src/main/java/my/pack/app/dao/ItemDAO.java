package my.pack.app.dao;

import javax.transaction.Transactional;

import my.pack.app.model.Item;

public interface ItemDAO {

	// Create
	@Transactional
	void insertItem(Item item);

	// Read

	// Update

	// Delete

}
