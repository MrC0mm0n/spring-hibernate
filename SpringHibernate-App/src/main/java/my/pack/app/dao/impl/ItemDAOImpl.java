package my.pack.app.dao.impl;

import my.pack.app.dao.ItemDAO;
import my.pack.app.model.Item;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemDAOImpl implements ItemDAO {

	@Autowired
	SessionFactory sessionFactory;

	public void insertItem(Item item) {
		sessionFactory.getCurrentSession().save(item);
	}

}
