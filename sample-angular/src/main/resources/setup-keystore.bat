openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
openssl rsa -passin pass:x -in server.pass.key -out server.key
openssl req -x509 -new -key server.key -out server.csr
openssl x509 -x509toreq -days 365 -in server.csr -signkey server.key -out server.req
openssl x509 -req -sha256 -extfile v3.ext -days 365 -in server.req -signkey server.key -out server.crt
openssl pkcs12 -export -in server.crt -inkey server.key -out server.p12
keytool -importkeystore -srckeystore server.p12 -destkeystore server.jks -srcstoretype pkcs12
keytool -importkeystore -srckeystore server.jks -destkeystore server.jks -deststoretype pkcs12
keytool -changealias -keystore server.jks -alias 1 -destalias servercert
keytool -list -rfc --keystore server.jks -storepass letmein
keytool -list -rfc --keystore server.jks | openssl x509 -inform pem -pubkey -noout
keytool -list -rfc --keystore server.jks | openssl x509 -inform pem -pubkey -noout > public-key.txt
del server.pass.key
del server.jks.old
del server.crt
del server.csr
del server.key
del server.p12
del server.req