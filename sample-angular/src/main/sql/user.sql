INSERT INTO `user` (`user_id`, `password`, `enabled`) VALUES
	('abc', '$2a$10$cEFlyJAlU7m9I6Jz8C3S3eHqhA/5IHEgICwnSgcLlsEwH4PVqNsvm', 1),
	('cde', '$2a$10$GYFtWaT7BKx3ArV5F.FrZOWBeKyfD.GQopVxvkdqNK4WaZm9l60UC', 1),
	('def', '$2a$10$qkYl2A.zhuWVElR0Xmvw9O1Kcwm6eSZwD5cImsXAeF4BXqGCwlAiK', 1),
	('ghi', '$2a$10$TCR82395nfQGnrZh7IMdzu4TOWz/M.27es94V3MDoGdwnuW1F/CVS', 1),
	('jkl', '$2a$10$7xVtiMzXizc8Lg62OiOijehk1sW5ZmfVHyNOAKBnvq/8p6KjUiyx2', 1),
	('mno', '$2a$10$pXiKgdxD/XBzrqaOdeX2T.2PpcFJYaDXjyRG6kNcwSAF92ylLyFay', 1),
	('pqr', '$2a$10$j0DNsD3le8nqrhO7jtqmXu0v72NoioBXSMA00KyZUjUTb5SAgfZT2', 1),
	('stu', '$2a$10$9hmrA8U2QbkugjqoimpVtuOw4b7zJg/jjJBL7VRfEPdVDLopLusI2', 1),
	('vxy', '$2a$10$w1G0TSCgYUexvPrrydbUbOchVXh7uGyJ0PzltLSUEjn0vpvOOvFXi', 1),
	('zab', '$2a$10$M3fAZtUX57VgAndXzE8y3eERx5KHHCaHX.IvBk6SerkeCIM3ERGzS', 1);
