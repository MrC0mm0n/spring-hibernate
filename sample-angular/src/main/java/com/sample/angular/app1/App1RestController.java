package com.sample.angular.app1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/app-1")
public class App1RestController {

	@GetMapping("/function-1")
	public String function1() {
		return "function-1-result";
	}

}
