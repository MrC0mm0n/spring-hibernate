package com.sample.angular.app3;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/app-3")
public class App3Controller {

	@GetMapping("")
	public String home(Model model) {
		return "app-3";
	}
	
	@GetMapping("/function-1")
    public String function1(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "app-3";
    }
	
}