$(document).ready(function(){	

	// Getting Ice-Rink dimentions
	var image_url = $('body').css('background-image').match(/^url\("?(.+?)"?\)$/);
	var	img = new Image();
	var img_width;
	var img_height;
	if (image_url[1]) {
		image_url = image_url[1];

		$(img).load(function () {
			console.log('Image resolution:' + img.width + 'x' + img.height);
			img_width = img.width;
			img_height = img.height;
			$('#ice-player-1').css('left', img_width/2);
			$('#ice-player-1').css('top', img_height/2);
		});

		img.src = image_url;
	}
	
	var socket = io.connect('http://build.kiwiwearables.com');

	socket.on('connect', function() {
		// Dell Tab 
		// socket.emit('listen', {device_id: '18:3B:D2:94:B9:B6', api_key: 'a64dee5684b24b408010bed011830e7f'});
		// LG Watch
		socket.emit('listen', {device_id: '34:4D:F7:F7:D8:06', api_key: 'a5d76092c1084c8b975bc6633ece698b'});
	});
	
	socket.on('listen_response', function(data) {
		var kiwiData = JSON.parse(data.message);
		console.log(kiwiData.ax + ', ' + kiwiData.ay + ', ' + kiwiData.az);
		tracking(kiwiData.ax, kiwiData.ay);
	});
	
	function tracking(a_x, a_y) {
	
		var player_1_left =  $('#ice-player-1').position().left;
		var player_1_top = $('#ice-player-1').position().top;
		var g_force = 9.8;
		// var sample_time = 0.1; // sec
		var sample_time = 0.02;
		var real_rink_width = 71.1;
		// Currently not using real_rink_height
		var real_rink_height = 32.8;
		var ascpt_ratio = (img_width/real_rink_width)*30;
		// var ascpt_ratio = (img_width/real_rink_width)*2.2394;
		// var ascpt_ratio = 37.795275591;
		
		var cal_px_x = 0;		
		var cal_pos_x = 0;
	
		var cal_px_y = 0;
		var cal_pos_y = 0;
		
		// Calculating px covered based on acceleration, using S=(g*t*t)/2, where u = 0
		if (a_x > 0) {
			// a_x is positive
			cal_px_x = ((a_x*g_force*sample_time*sample_time*100)/(2))*ascpt_ratio;
			cal_pos_x = player_1_left + cal_px_x;
		} else if (a_x < 0) {
			// a_x is negative
			cal_px_x = ((-1*a_x*g_force*sample_time*sample_time*100)/(2))*ascpt_ratio;
			cal_pos_x = player_1_left - cal_px_x;
		}
		
		if (a_y > 0) {
			// a_y is positive
			cal_px_y = ((a_y*g_force*sample_time*sample_time*100)/(2))*ascpt_ratio;
			cal_pos_y = player_1_top - cal_px_y;
		} else if (a_y < 0) {
			// a_y is negative
			cal_px_y = ((-1*a_x*g_force*sample_time*sample_time*100)/(2))*ascpt_ratio;
			cal_pos_y = player_1_top + cal_px_y;
		}
		
		cal_pos_x = Math.round(cal_pos_x);
		cal_pos_y = Math.round(cal_pos_y);
	
		// Making changes to player movemnets within the rink
		if((cal_pos_x >= 0 && cal_pos_x <= img_width) 
			&& (cal_pos_y >= 0 && cal_pos_y <= img_height)) {
			// console.log(cal_pos_x + ', ' + cal_pos_y);
			 
			$('#ice-player-1').animate({
				left: cal_pos_x,
				top: cal_pos_y				
			}, sample_time*1000, function() {				
				$('#coord-player-1').text($('#ice-player-1').position().left + ', ' + $('#ice-player-1').position().top);				
			});
			
			$('#ice-player-1').css('left', cal_pos_x);
			$('#ice-player-1').css('top', cal_pos_y);
			
			// count = count + 1;
		}
	}
	
	$('#player-monitor-table').dynatable();
  
});