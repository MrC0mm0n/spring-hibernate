<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home</title>
<!-- Adding favicon -->
<link rel="icon" href="resources/images/Logo.ico">

<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional Bootstrap theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- JQuery UI CSS -->
<link rel="stylesheet"
	href="resources/jquery-ui-1.11.2/jquery-ui.min.css">
<link rel="stylesheet"
	href="resources/jquery-ui-1.11.2/jquery-ui.structure.min.css">
<link rel="stylesheet"
	href="resources/jquery-ui-1.11.2/jquery-ui.theme.min.css">

<!-- Custom CSS for page -->
<link rel="stylesheet" href="resources/css/home.css">

</head>
<body>

	<div class="site-wrapper">
		<div class="site-wrapper-inner">
			<div class="cover-container">

				<div class="masthead clearfix">
					<div class="inner">
						<h3 class="masthead-brand">
							<a href="about">M / R</a>
						</h3>
						<nav>
							<ul class="nav masthead-nav">
								<li><a href="about">About</a></li>
								<li class="active"><a href="portfolio">Portfolio</a></li>
								<li><a href="resources/files/Resume.pdf" target="_blank">Resume</a></li>
							</ul>
						</nav>
					</div>
				</div>

				<div class="inner cover">
					<table class="table">
						<thead align="left">
							<tr>
								<th>Projects</th>
								<th>About</th>
								<th>Links</th>
							</tr>
						</thead>
						<tbody align="left">
							<tr>
								<td>Search Engine</td>
								<td>The objective of this project is to create a semantic
									engine that 'IDEALLY' processes all natural languages and
									answers user questions by searching open source data sources,
									i.e. Wikipedia, DBpedia and others.</td>
								<td><p>
										<a href="http://integ.cfapps.io/" target="_blank">Try it!</a>
									</p>
									<p>
										<a
											href="https://bitbucket.org/MrC0mm0n/opendbintegration/overview"
											target="_blank">Source code</a>
									</p></td>
							</tr>
							<tr>
								<td>Information Collection System</td>
								<td>This is a demo project to show the working of a cloud
									server and cloud database to collect user information.</td>
								<td><p>
										<a href="http://infocollectsystem.cfapps.io/" target="_blank">Try
											it!</a>
									</p>
									<p>
										<a href="https://bitbucket.org/MrC0mm0n/spring-hibernate"
											target="_blank">Source code</a>
									</p></td>
							</tr>
							<tr>
								<td>My Visual Home page</td>
								<td>This project is a node-edge based representation of my
									home page.</td>
								<td><p>
										<a href="visual-web" target="_blank">Try it!</a>
									</p>
									<p>
										<a href="https://bitbucket.org/MrC0mm0n/spring-hibernate"
											target="_blank">Source code</a>
									</p></td>
							</tr>
							<tr>
								<td>The Sensor Web</td>
								<td>This is a project I came up with at the <a
									href="https://uofthacks.com/" target="_blank">UofTHacks
										2015</a>. I gather physical parameters(sensory data) and make this
									available on a web application, and also as a report via SMS
									and email.
								</td>
								<td><p>
										<a href="http://monitor-pivotal.cfapps.io/" target="_blank">Try
											it!</a>
									</p>
									<p>
										<a href="https://bitbucket.org/MrC0mm0n/sensor-web"
											target="_blank">Source code</a>
									</p></td>
							</tr>
							<tr>
								<td>Track the Player</td>
								<td>This is a project I came up with at the <a
									href="http://www.sportshackweekend.org/" target="_blank">SportsHack
										2014</a>. I track a player in an ice-rink using a smart-watch.
								</td>
								<td><p>
										<a href="sportshack" target="_blank">Try it!</a>
									</p>
									<p>
										<a href="https://bitbucket.org/MrC0mm0n/spring-hibernate/"
											target="_blank">Source code</a>
									</p></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="mastfoot">
					<div class="inner">
						<p></p>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- JQuery UI library -->
	<script type="text/javascript"
		src="resources/jquery-ui-1.11.2/jquery-ui.min.js"></script>
	<!-- Latest compiled and minified Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<!-- Code to run in page -->
	<script type="text/javascript" src="resources/js/home.js"></script>
</body>
</html>
