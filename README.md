# README #

The objective of this project is to setup a basic Spring-based projects.

### How do I get set up? ###

You can set this project up by looking at the issue #1: Project setup

### Requirements ###
* [Eclipse IDE(Java EE)](https://www.eclipse.org/downloads/)
* [Java 7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
* [Spring 4](https://spring.io/)
* [Hibernate 4](http://hibernate.org/)
* [Bootstrap UI](http://getbootstrap.com/)
* [DynaTable](http://www.dynatable.com/)
* [MySQL Community Edition](http://www.mysql.com/products/community/)

### Who do I talk to? ###

* The admin of this repo can be reached at inform.smohdr88@gmail.com