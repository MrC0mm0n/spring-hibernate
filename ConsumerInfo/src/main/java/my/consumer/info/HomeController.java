package my.consumer.info;

import my.consumer.info.doa.CustomerDAO;
import my.consumer.info.model.Customer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */

	@Autowired
	CustomerDAO customerDAO;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home() {
		logger.info("Entering HomeController.home");

		ModelAndView modelView = new ModelAndView("home");
		modelView.addObject("customerInfo", new Customer());
		modelView.addObject("customerList", customerDAO.getCustomers());

		logger.info("Exiting HomeController.home");
		return modelView;
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(ModelAndView modelView, Customer customer) {
		logger.info("Entering HomeController.save");
		modelView.setViewName("home");

		if (customerDAO.isCustomerExist(customer))
			customerDAO.updateCustomer(customer);
		else
			customerDAO.insertCustomer(customer);

		modelView.addObject("customerInfo", new Customer());
		modelView.addObject("customerList", customerDAO.getCustomers());

		logger.info("Exiting HomeController.save");
		return modelView;
	}

	@RequestMapping(value = "update", method = RequestMethod.GET)
	public ModelAndView update(ModelAndView modelView,
			@RequestParam(value = "id") int id) {
		logger.info("Entering HomeController.update");
		modelView.setViewName("home");

		modelView.addObject("customerInfo", customerDAO.getById(id));
		modelView.addObject("customerList", customerDAO.getCustomers());

		logger.info("Exiting HomeController.update");
		return modelView;
	}

	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public ModelAndView delete(ModelAndView modelView,
			@RequestParam(value = "id") int id) {
		logger.info("Entering HomeController.delete");
		modelView.setViewName("home");

		customerDAO.deleteCustomer(id);
		modelView.addObject("customerInfo", new Customer());
		modelView.addObject("customerList", customerDAO.getCustomers());

		logger.info("Exiting HomeController.delete");
		return modelView;
	}

	// Ajax method
	@RequestMapping(value = "util")
	public @ResponseBody String util() {
		logger.info("Entering HomeController.util");

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		logger.info("Exiting HomeController.util");

		return "Util Method reached";
	}

}
