package my.consumer.info.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
@EnableAsync
public class Util {
	private static final Logger logger = LoggerFactory.getLogger(Util.class);

	// Documentation:
	// http://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/scheduling.html

	@Scheduled(fixedRate = 7000)
	public void scheduledMehtod() {
		logger.info("Entering Util.scheduledMehtod");

		logger.info("Running scheduled task");
		// asyncMehtod();

		logger.info("Exiting Util.scheduledMehtod");
	}

	@Async
	public void asyncMehtod() {
		logger.info("Entering Util.asyncMehtod");

		logger.info("Running Async task");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		logger.info("Exiting Util.asyncMehtod");
	}

}
