package my.consumer.info.doa.impl;

import java.util.List;

import my.consumer.info.doa.CustomerDAO;
import my.consumer.info.model.Customer;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerDAOImpl implements CustomerDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// Create
	public void insertCustomer(Customer customer) {
		sessionFactory.getCurrentSession().save(customer);
	}

	// Read
	public Customer getById(int id) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Customer.class);
		criteria.add(Restrictions.eq("id", id));
		return (Customer) criteria.list().get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Customer> getCustomers() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Customer.class);
		return criteria.list();
	}

	public boolean isCustomerExist(Customer customer) {
		boolean flag = false;
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Customer.class);
		criteria.add(Restrictions.eq("id", customer.getId()));
		if (criteria.list().size() != 0)
			flag = true;
		return flag;
	}

	// Update
	public void updateCustomer(Customer customer) {
		Session session = sessionFactory.getCurrentSession();
		Customer existCustomer = (Customer) session
				.createCriteria(Customer.class)
				.add(Restrictions.eq("id", customer.getId())).list().get(0);
		existCustomer.setCustomer_address(customer.getCustomer_address());
		existCustomer.setCustomer_name(customer.getCustomer_name());
		existCustomer.setCustomer_id(customer.getCustomer_id());
	}

	// Delete
	public void deleteCustomer(int id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Customer.class);
		criteria.add(Restrictions.eq("id", id));
		session.delete((Customer) criteria.list().get(0));
	}

}
