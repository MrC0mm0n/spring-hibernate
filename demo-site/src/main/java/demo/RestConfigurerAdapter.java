package demo;

import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class RestConfigurerAdapter extends RepositoryRestConfigurerAdapter {

	@PersistenceContext
	EntityManager entityManagerFactory;

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {

		// config.exposeIdsFor(ClassName.class);

		// ###################
		config.exposeIdsFor(entityManagerFactory.getMetamodel().getEntities().stream().map(e -> e.getJavaType()).collect(Collectors.toList()).toArray(new Class[0]));

		// Use to specifically expose ids of classes
		// config.exposeIdsFor(ClassName.class);

	}

}