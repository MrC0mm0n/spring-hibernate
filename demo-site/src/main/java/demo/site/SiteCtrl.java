package demo.site;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import demo.repository.FruitRepository;

@Controller
@RequestMapping(value = "/")
public class SiteCtrl {

	private final static Logger logger = LoggerFactory.getLogger(SiteCtrl.class);

	@Autowired
	FruitRepository fruitRepo;

	@GetMapping("")
	public ModelAndView home(ModelAndView modelView) {
		logger.debug(">> home()");

		modelView.addObject("greeting", "Hello World!");

		modelView.setViewName("index");

		logger.debug("<< home()");
		return modelView;
	}

}