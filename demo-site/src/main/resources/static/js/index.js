$(document).ready(function(){
	debug('>> ready()');
	
	getFruits();
	
	debug('<< ready()');
});

function getFruits() {
	debug('>> getFruits()');
	
	var uri = '/api/fruits';
	
	$.get({
		url : uri,
		success : function(response) {
			
			debug(response._embedded['fruits']);
			
			$('#table-fruits').DataTable({
				paginationType: 'full_numbers',
				lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
				dom: 'lBfrtip',
				data: response._embedded['fruits'],
				buttons: {
					dom: {
						button: {
							className: 'btn btn-outline-dark btn-sm'
						}
					}
				},
				columns: [
					{ data: 'id', defaultContent: '' },
					{ data: 'name', defaultContent: '' }
				]
			});
			$('input[type=search]').attr('placeholder', 'Search filtered results');
			
		},
		error: function(request, status, error) {
			debug(request);
			debug(status);
			debug(error);
		}
	});
	
	debug('<< getFruits()');
}