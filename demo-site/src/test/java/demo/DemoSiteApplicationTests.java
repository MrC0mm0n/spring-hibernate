package demo;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import demo.model.Fruit;
import demo.repository.FruitRepository;

@SpringBootTest
class DemoSiteApplicationTests {

	@Autowired
	FruitRepository fruitRepo;

	@Test
	void test() {

		List<Fruit> fruits = new ArrayList<Fruit>();

		Fruit fruit = new Fruit();
		fruit.setName("Mandarin");
		fruits.add(fruit);

		fruit = new Fruit();
		fruit.setName("Mangosteen");
		fruits.add(fruit);

		fruit = new Fruit();
		fruit.setName("Nectarine");
		fruits.add(fruit);

		fruit = new Fruit();
		fruit.setName("Orange");
		fruits.add(fruit);

		fruit = new Fruit();
		fruit.setName("Persimmon – Japanese");
		fruits.add(fruit);

		fruit = new Fruit();
		fruit.setName("Pitaya (Dragonfruit)");
		fruits.add(fruit);

		fruit = new Fruit();
		fruit.setName("Plums");
		fruits.add(fruit);

		fruit = new Fruit();
		fruit.setName("Quince");
		fruits.add(fruit);

		fruit = new Fruit();
		fruit.setName("Rose-Apple");
		fruits.add(fruit);

		fruit = new Fruit();
		fruit.setName("Tamarind");
		fruits.add(fruit);

		fruitRepo.saveAll(fruits);

	}

}