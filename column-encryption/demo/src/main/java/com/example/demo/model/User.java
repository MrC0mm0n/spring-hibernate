package com.example.demo.model;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.example.demo.util.AttributeEncryptorInteger;
import com.example.demo.util.AttributeEncryptorString;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class User {

	@Id
	@GeneratedValue
	Long id;

	@Convert(converter = AttributeEncryptorString.class)
	String name;

	String email;

	@Convert(converter = AttributeEncryptorInteger.class)
	Integer ssn;

}
