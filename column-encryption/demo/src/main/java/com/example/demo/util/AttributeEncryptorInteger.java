package com.example.demo.util;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.AttributeConverter;

import org.springframework.stereotype.Component;

@Component
public class AttributeEncryptorInteger implements AttributeConverter<Integer, String> {

	private static final String AES = "AES";
	private static final String SECRET = "secret-key-12345";

	private final Key key;
	private final Cipher cipher;

	public AttributeEncryptorInteger() throws Exception {
		key = new SecretKeySpec(SECRET.getBytes(), AES);
		cipher = Cipher.getInstance(AES);
	}

	@Override
	public Integer convertToEntityAttribute(String dbData) {
		try {
			cipher.init(Cipher.DECRYPT_MODE, key);
			return new BigInteger(cipher.doFinal(Base64.getDecoder().decode(dbData))).intValue();
		} catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public String convertToDatabaseColumn(Integer attribute) {
		try {
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return Base64.getEncoder().encodeToString(cipher.doFinal(BigInteger.valueOf(attribute).toByteArray()));
		} catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
			throw new IllegalStateException(e);
		}
	}

}
