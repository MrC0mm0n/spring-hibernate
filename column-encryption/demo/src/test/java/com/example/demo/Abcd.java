package com.example.demo;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Abcd {

	private static final Logger logger = LoggerFactory.getLogger(Abcd.class);

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		logger.info("-- setUpBeforeClass");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		logger.info("-- tearDownAfterClass");
	}

	@BeforeEach
	void setUp() throws Exception {
		logger.info("-- setUp");
	}

	@AfterEach
	void tearDown() throws Exception {
		logger.info("-- tearDown");
	}

	@Test
	void test() {
		logger.info("-- test");
	}

	@Test
	void test1() {
		logger.info("-- test1");
	}

}
