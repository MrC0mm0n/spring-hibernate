package standalone.db.app;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

@SuppressWarnings("restriction")
public class MainController extends Application {

	static Logger logger = Logger.getLogger(MainController.class.toString());

	public static void main(String[] args) {
		logger.info("Staring application");

		launch(args);

		logger.info("Shutting down application");
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Standalone DB Application");

		VBox root = new VBox();

		// Create function
		Button btnCreate = new Button();
		btnCreate.setText("Add");
		btnCreate.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				logger.info("Adding Info");
				String query = "INSERT INTO customer "
						+ "(ID,CUSTOMER_ADDRESS,CUSTOMER_ID,CUSTOMER_NAME)"
						+ " VALUES('66','Russia','34342','Kirischian')";
				DBQuery(true, query);
			}
		});
		root.getChildren().add(btnCreate);

		// Read function
		Button btnRead = new Button();
		btnRead.setText("Read/Get All");
		btnRead.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				logger.info("Reading Info");
				String query = "SELECT * FROM customer";
				DBQuery(false, query);
			}
		});
		root.getChildren().add(btnRead);

		// Update function
		Button btnUpdate = new Button();
		btnUpdate.setText("Update");
		btnUpdate.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				logger.info("Updating Info");
				String query = "UPDATE customer "
						+ "SET CUSTOMER_ADDRESS='China',CUSTOMER_ID='88',CUSTOMER_NAME='Jackson'"
						+ " WHERE ID=66;";
				DBQuery(true, query);
			}
		});
		root.getChildren().add(btnUpdate);

		// Delete function
		Button btnDelete = new Button();
		btnDelete.setText("Delete");
		btnDelete.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				logger.info("Deleting Info");
				String query = "DELETE FROM customer WHERE ID=66";
				DBQuery(true, query);
			}
		});
		root.getChildren().add(btnDelete);

		root.setAlignment(Pos.CENTER);
		root.setSpacing(10);

		Scene scene = new Scene(root, 360, 270);
		// Scene scene = new Scene(root, 480, 360);
		// Scene scene = new Scene(root, 640, 480);
		stage.setScene(scene);
		stage.show();
	}

	void DBQuery(boolean dataManipulate, String query) {
		MysqlDataSource dbConfig = new MysqlDataSource();
		dbConfig.setServerName("192.168.1.11");
		dbConfig.setDatabaseName("test");
		dbConfig.setUser("root");
		dbConfig.setPassword("root");
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Integer iRs = 0;
		try {
			con = dbConfig.getConnection();
			stmt = con.createStatement();

			if (dataManipulate) {
				iRs = stmt.executeUpdate(query);
				if (iRs == 0) {
					logger.info("Operation Failed");
				} else {
					logger.info("Operation Successful");
				}
			} else {
				rs = stmt.executeQuery(query);
				while (rs.next()) {
					System.out.println(rs.getString("ID") + ", "
							+ rs.getString("CUSTOMER_ADDRESS") + ", "
							+ rs.getString("CUSTOMER_ID") + ", "
							+ rs.getString("CUSTOMER_NAME"));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
