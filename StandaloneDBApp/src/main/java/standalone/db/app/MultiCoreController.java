package standalone.db.app;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Logger;

public class MultiCoreController {

	static Logger logger = Logger.getLogger(MainController.class.toString());

	public static void main(String[] args) {
		logger.info("Starting application");

		ForkJoinPool pool = new ForkJoinPool();

		logger.info("Total threads available on system: "
				+ pool.getParallelism());

		MultiCoreRecursiveAction task1 = new MultiCoreRecursiveAction("Task 1",
				2);
		pool.execute(task1);

		MultiCoreRecursiveAction task2 = new MultiCoreRecursiveAction("Task 2",
				4);
		pool.execute(task2);

		MultiCoreRecursiveAction task3 = new MultiCoreRecursiveAction("Task 3",
				5);
		pool.execute(task3);

		MultiCoreRecursiveAction task4 = new MultiCoreRecursiveAction("Task 4",
				7);
		pool.execute(task4);

		task1.join();
		task2.join();
		task3.join();
		task4.join();

		pool.shutdown();

		logger.info("Closing application");
	}
}

@SuppressWarnings("serial")
class MultiCoreRecursiveAction extends RecursiveAction {

	Logger logger = Logger.getLogger(MultiCoreRecursiveAction.class.toString());

	String taskName;
	Integer duration;

	MultiCoreRecursiveAction(String taskName, Integer duration) {
		this.taskName = taskName;
		this.duration = duration;
	}

	@Override
	protected void compute() {
		logger.info("Started " + taskName);
		try {
			Thread.sleep(duration * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		logger.info("Ended " + taskName + ", slept " + duration + " sec");
	}

}

@SuppressWarnings("serial")
class MultiCoreRecursiveTask extends RecursiveTask<Boolean> {

	@Override
	protected Boolean compute() {

		return true;
	}

}